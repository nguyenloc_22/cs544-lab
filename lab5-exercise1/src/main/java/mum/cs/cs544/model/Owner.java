package mum.cs.cs544.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "owner")
public class Owner {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "name", length = 255, nullable = false)
	private String name;
	
	@Column(name = "address", length = 255, nullable = false)
	private String address;
	
	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	private List<Car> cars;
	
	public Owner() {
		cars = new ArrayList<Car>();
	}
	
	public Owner(String name, String address) {
		this();
		
		this.name = name;
		this.address = address;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public List<Car> getCars() {
		return cars;
	}
	
	public void addCar(Car car) {
		this.cars.add(car);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
