package mum.cs.cs544;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import mum.cs.cs544.model.Car;
import mum.cs.cs544.model.Owner;
import mum.cs.cs544.util.TraceService;
import mum.cs.cs544.util.TraceServiceImpl;

/**
 * Hello world!
 *
 */
public class App {
	
	private final String DemoDbPu = "cs544.lab5.pu";
	private final EntityManagerFactory EntityManagerFactory = 
    		Persistence.createEntityManagerFactory(DemoDbPu);
	
	private TraceService traceService = new TraceServiceImpl();
	
	public static void main(String[] args) {
		new App().runApp();
	}
	
	private void runApp() {
		
		traceService.trace("Start Lab5.");
		
		addCarWithOwner();
		loadCars();
	}
	
	private void addCarWithOwner() {
		EntityManager em = EntityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        Owner boss = new Owner("admin", "1000 4th, Fairfield, IA.");
        //em.persist(owner);
        
        // Create new instance of Car and set values in it
        Car car1 = new Car("BMW", "SDA231", 30221.00);
        car1.setOwner(boss);
        
        boss.addCar(car1);
        em.persist(boss);

        em.getTransaction().commit();
        em.close();
	}
	
	private void loadCars() {
		EntityManager em = EntityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        // retieve all cars
        TypedQuery<Car> query = em.createQuery("from Car", Car.class);
        List<Car> carList = query.getResultList();
        for (Car car : carList) {
            traceService.trace("brand= " + car.getBrand() + ", year= "
                    + car.getYear() + ", price= " + car.getPrice());
        }
        em.getTransaction().commit();
        em.close();
	}
	
	@SuppressWarnings("unused")
	private void runApp1() {
		
		traceService.trace("Start Lab5.");
		
		EntityManager em = EntityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        Owner owner = new Owner("admin", "1000 4th, Fairfield, IA.");
        em.persist(owner);
        
        // Create new instance of Car and set values in it
        Car car1 = new Car("BMW", "SDA231", 30221.00);
        car1.setOwner(owner);
        
        // save the car
        em.persist(car1);
        //em.merge(car1);
        // Create new instance of Car and set values in it
        Car car2 = new Car("Mercedes", "HOO100", 4088.00);
        // save the car
        em.persist(car2);

        em.getTransaction().commit();
        em.close();

        em = EntityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        // retieve all cars
        TypedQuery<Car> query = em.createQuery("from Car", Car.class);
        List<Car> carList = query.getResultList();
        for (Car car : carList) {
            traceService.trace("brand= " + car.getBrand() + ", year= "
                    + car.getYear() + ", price= " + car.getPrice());
        }
        em.getTransaction().commit();
        em.close();
	}
}
