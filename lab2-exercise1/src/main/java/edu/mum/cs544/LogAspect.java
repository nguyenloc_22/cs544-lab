package edu.mum.cs544;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
	private static Logger logger = 
			LogManager.getLogger(LogAspect.class.getName());

	// indicate any return type
	//any method name
	//any parameters
//	@Before("execution(* *(..))")
//	public void logBefore(JoinPoint joinpoint) {
//		logger.warn("About to exec: " + joinpoint.getSignature().getName());
//	}

	@After("execution(* edu.mum.cs544.EmailSender.sendEmail(..))")
	public void logAfterSendEmail(JoinPoint joinpoint) {
		Object[] args = joinpoint.getArgs();
		String email = args[0].toString();
		String message = args[1].toString();
		
		IEmailSender target = (IEmailSender)joinpoint.getTarget();
		String outgoingMailServer = target.getOutgoingMailServer();
		
		logger.warn("\n" +LocalDateTime.now().toString() 
				+ "\nmethod = " + joinpoint.getSignature().getName()
				+ "\naddress = " + email
				+ "\nmessage = " + message
				+ "\noutgoing mail server = " + outgoingMailServer);
	}

//	@Before("execution(* *(..))")
//	public void logTargetBefore(JoinPoint joinpoint) {
//		logger.warn("About to exec a method on: " + joinpoint.getTarget());
//	}
//	@After("execution(* *(..))")
//	public void logTargetAfter(JoinPoint joinpoint) {
//		logger.warn("Just execed a method on: " + joinpoint.getTarget());
//	}

}
