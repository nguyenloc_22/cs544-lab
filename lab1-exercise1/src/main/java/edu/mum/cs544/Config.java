package edu.mum.cs544;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;

@Configurable
@ComponentScan("edu.mum.cs544")
public class Config {
}
