package edu.mum.cs544;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		// 1A Using Java Config
//		 ApplicationContext context = 
//				new AnnotationConfigApplicationContext(Config.class);

		// 1B Using xml config
		// ApplicationContext context = 
		//		new ClassPathXmlApplicationContext("springconfig.xml");
		
		// 1C Using xml config - No annotation

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("springconfig1.xml");

		Greeting greeting = context.getBean("greeting", Greeting.class);
		greeting.sayHello();
		

		
		
		
	}
	
}
