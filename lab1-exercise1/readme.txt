CREATE USER 'cs544'@'localhost' IDENTIFIED BY 'cs544';
GRANT ALL ON [table_name].* TO 'cs544'@'localhost';


create database cs544_student;
GRANT ALL ON cs544_student.* TO 'cs544'@'localhost';