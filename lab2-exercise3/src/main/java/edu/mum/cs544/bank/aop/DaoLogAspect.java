package edu.mum.cs544.bank.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import edu.mum.cs544.bank.logging.ILogger;

@Aspect
@Component
public class DaoLogAspect {
	
	@Autowired
	private ILogger logger;
	
	@Before("execution(* edu.mum.cs544.bank.dao.*.*(..))")
	public void beforeDaoApi(JoinPoint jp) throws Throwable { 
		
		String method = jp.getSignature().getName();
		logger.log("BeforeDAO: to execute "+ method); 
	}
	
	@Around("execution(* edu.mum.cs544.bank.dao.*.*(..))")
	public Object invoke(ProceedingJoinPoint call ) throws Throwable { 
		StopWatch sw = new StopWatch();
		
		String method = call.getSignature().getName();
		sw.start(method);
		
		Object retVal = call.proceed();
		sw.stop();
		long totaltime = sw.getLastTaskTimeMillis();
		
		
		logger.log("Time to execute "+ method +" = " + totaltime + " ms");
		
		return retVal; 
	}
}
