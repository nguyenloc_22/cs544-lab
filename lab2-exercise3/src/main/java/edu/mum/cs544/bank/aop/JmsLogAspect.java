package edu.mum.cs544.bank.aop;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.mum.cs544.bank.logging.ILogger;

@Aspect
@Component
public class JmsLogAspect {
	
	@Autowired
	private ILogger logger;

	@After("execution(* edu.mum.cs544.bank.jms.JMSSender.sendJMSMessage(..))")
	public void logAfterSendEmail(JoinPoint joinpoint) {
		Object[] args = joinpoint.getArgs();
		String message = args[0].toString();
		
		logger.log("\n" +LocalDateTime.now().toString() 
				+ "\nmethod = " + joinpoint.getSignature().getName()
				+ "\nmessage = " + message + " is sent.");
	}
	
}
