package mum.cs.cs544;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@Entity
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	@ManyToMany
	@JoinTable(name = "category_items",
			joinColumns = { @JoinColumn(name = "category_id") },
			inverseJoinColumns = { @JoinColumn(name = "item_id") })
	private List<Item> items;
	
	public Category(String name) {
		this.name = name;
		this.items = new ArrayList<Item>();
	}
}
