package mum.cs.cs544;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@Entity
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String comment;
	
	@ManyToOne
	@JoinTable(name = "comment_item",
		joinColumns = { @JoinColumn(name = "comment") },
		inverseJoinColumns = { @JoinColumn(name = "item") }
	)
	private Item item;
	
	@ManyToOne
	@JoinColumn(name = "user_USER_ID")
	private User user;
	
	public Comment(String comment) {
		this.comment = comment;
	}
}
