package mum.cs.cs544;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//@Entity
public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 842601082833194527L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	@ManyToMany(mappedBy = "items")
	private List<Category> categories;
	
	@OneToMany(mappedBy = "item")
	private List<Comment> comments;
	
	public Item(String name) {
		this.name = name;
		this.categories = new ArrayList<Category>();
	}
}
