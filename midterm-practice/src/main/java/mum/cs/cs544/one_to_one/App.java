package mum.cs.cs544.one_to_one;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 
drop table if exists Course_Grades;
drop table if exists Student_Grades;
drop table if exists student_enrollment;
drop table if exists course;
drop table if exists grades;
drop table if exists student;


 * @author locnv
 *
 */
public class App {
	private String DemoDbPu = "cs544";
	private EntityManagerFactory emf;
	
	public static void main(String[] args) {
		new App().runApp();
	}
	
	private void runApp() {
		trace("[app] -> Starting application......");
		emf = Persistence.createEntityManagerFactory(DemoDbPu);
		
		test1();
		
		emf.close();
		trace("[app] -> Done!");
	}

	private void test1() {
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Address addr1 = new Address("Fairfield");
		
		Customer cus1 = new Customer("Eugene");
		cus1.setAddress(addr1);
		
		em.persist(cus1);
		addr1.setId(cus1.getId());
		
		em.getTransaction().commit();
		em.close();
	}
	
	private void trace(String msg) {
		System.out.println(msg);
	}
}
