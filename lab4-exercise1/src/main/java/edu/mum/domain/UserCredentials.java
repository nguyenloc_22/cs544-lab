package edu.mum.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authentication")
public class UserCredentials {
	
	@Id
	@Column(name = "user", length = 127, nullable = false)
	private String username;
	
	@Column(name = "password", length = 32, nullable = false)
	private String password;
	
	@Column(name = "enabled", length = 1, nullable = true)
	private Boolean enabled;

	public UserCredentials() {
		
	}
	
	public UserCredentials(
		String username, String password, Boolean enabled) {
		super();
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return String.format("UserCredential {"
				+ " username: %s,"
				+ " password: %s,"
				+ " enabled: %d"
				+ "}", username, password, enabled);

	}
}
