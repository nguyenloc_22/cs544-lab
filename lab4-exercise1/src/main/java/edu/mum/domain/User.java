package edu.mum.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
//import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "users")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5288342383066238097L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id", length = 20, nullable = false)
	private Long id;

	@Column(name = "firstname", length = 255, nullable = false)
	private String firstName;

	@Column(name = "lastname", length = 255, nullable = false)
	private String lastName;

	@Column(name = "email", length = 255, nullable = false)
	private String email;

	@Column(name = "rating", length = 11, nullable = false)
	private int rating = 0;

	@Column(name = "is_admin", length = 1, nullable = false)
	private boolean admin = false;

	@Version
	@Column(name = "version", length = 11, nullable = false)
	private int version = 0;

	@Temporal(TemporalType.DATE)
	@Column(name = "lastlogin", nullable = true)
	private Date lastLogin;
	
	public User() {
		
	}
	
	public User(
			String firstName, String lastName, String email, 
			int rating, boolean admin, int version, Date lastLogin) {
		
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.rating = rating;
		this.admin = admin;
		this.version = version;
		this.lastLogin = lastLogin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstname) {
		this.firstName = firstname;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastname) {
		this.lastName = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@Override
	public String toString() {
		String lastLoginStr = (lastLogin != null) ? lastLogin.toString() : "";
		return String.format("User: {"
				+ " id: %d,"
				+ " firstName: %s,"
				+ " lastName: %s,"
				+ " email: %s,"
				+ " rating: %d,"
				+ " isAdmin: %b,"
				+ " version: %d,"
				+ " lastLogin: %s"
				+ "}", 
				id, firstName, lastName, email, rating, admin, version, lastLoginStr);
	}
	
}