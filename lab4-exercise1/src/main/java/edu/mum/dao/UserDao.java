package edu.mum.dao;

import javax.persistence.EntityManager;

import org.h2.message.DbException;

import edu.mum.domain.User;

public interface UserDao extends GenericDao<User> {
 
	/**
	 * Find user by email
	 * @param email
	 * @return
	 * @throws DbException
	 */
	User findByEmail(String email) throws DbException;
	
	EntityManager getEntityManager();
}
