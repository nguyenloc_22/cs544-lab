package edu.mum.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.h2.message.DbException;
import org.springframework.stereotype.Repository;

import edu.mum.dao.UserDao;
import edu.mum.domain.User;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {
	
	public UserDaoImpl() {
		setDaoType(User.class);
	}

	/**
	 * Find by email
	 */
	public User findByEmail(String email) throws DbException {

		try {
			Query query = entityManager.createQuery(
					"select u from User u  where u.email =:email");
			
			return (User) query
					.setParameter("email", email)
					.getSingleResult();
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
