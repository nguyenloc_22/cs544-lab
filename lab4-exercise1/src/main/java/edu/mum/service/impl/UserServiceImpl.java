package edu.mum.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.mum.dao.UserDao;
import edu.mum.domain.User;
import edu.mum.util.TraceService;

@Service
@Transactional
public class UserServiceImpl implements edu.mum.service.UserService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private TraceService traceService;

	public void save(User user) {
		userDao.save(user);
	}

	@Override
	public void update(User user) {
		userDao.update(user);
	}
	
	public List<User> findAll() {
		return (List<User>) userDao.findAll();
	}

	public User findByEmail(String email) {
		try {
			return userDao.findByEmail(email);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	@Override
	@Transactional
	public void testFlushRefresh() {
		EntityManager em = userDao.getEntityManager();
		
		traceService.trace("Test Flush / Refresh");
		
		User user = new User("udemo", "u", "udemo@gmail.com", 1, false, 1, null);
		
		//em.getTransaction().begin();
		traceService.trace("1");
		em.persist(user);
		traceService.trace("2");
		em.remove(user);
		traceService.trace("3");
		em.flush();
		traceService.trace("4");
		
		TypedQuery<User> q = em.createQuery("from User", User.class); 
		traceService.trace("5");
		
		List<User> users = q.getResultList();
		traceService.trace(users.toString());
		traceService.trace("6"); 
		// em.getTransaction().commit(); 
		traceService.trace("7");
		
		
	}

}
