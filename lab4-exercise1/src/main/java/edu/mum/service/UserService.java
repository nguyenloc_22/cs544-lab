package edu.mum.service;

import java.util.List;

import edu.mum.domain.User;
 
public interface UserService {

	void save(User user);
	List<User> findAll();
	void update(User user);
	User findByEmail(String email);
	void testFlushRefresh();
}
