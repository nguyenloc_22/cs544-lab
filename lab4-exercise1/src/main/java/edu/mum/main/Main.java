package edu.mum.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import edu.mum.domain.User;
import edu.mum.service.UserService;
import edu.mum.util.TraceService;

/**
 * JPA API demonstration
 * @author locnv
 *
 */
@Component
public class Main {
	
	@Autowired
	private TraceService traceService;
	
	@Autowired
	private UserService userService;

	public static void main(String[] args) {
		
		ApplicationContext ctx =
			new ClassPathXmlApplicationContext("context/applicationContext.xml");
		ctx.getBean(Main.class).runApp(ctx);
	}
	
	private void runApp(ApplicationContext ctx) {
		traceService.trace("Start applicaiton.");
		
		demo001();
		demo002();
		demo003();
	}

	/**
	 *
	 * Exercise the Flush/Refresh capability.
	 */
	private void demo003() {
		traceService.nl();
		traceService.trace("Demo 003: Flush / Refresh");
		
		userService.testFlushRefresh();
	}
	/**
	 * Exercise the Merge capability:  
	 * - Create an entity, Update entity; 
	 * - try updating the entity again with the “unmanaged” version of the entity. 
	 * Expected result: StaleObjectStateException.
	 */
	private void demo002() {
		traceService.nl();
		traceService.trace("Demo 002: Create / update entity / update with unmanaged");
		
		User u = new User("John", "Smith", "sjohn@gmail.com", 1, true, 1, null);
		userService.save(u);
		
		traceService.trace(
			String.format("Added user: %s %s into db.", u.getFirstName(), u.getLastName()));
		
		User dbUser = userService.findByEmail("sjohn@gmail.com");
		if(dbUser != null) {
			traceService.trace(
				String.format("Find user by email: %s %s.", u.getFirstName(), u.getLastName()));
		} else {
			traceService.trace("Failed to find user by email.");
			return;
		}
		
		dbUser.setFirstName("hano");
		userService.update(dbUser);
		
		// Find after update
		dbUser = userService.findByEmail("sjohn@gmail.com");
		if(dbUser != null) {
			traceService.trace(
				String.format("Find user by email (after updated): %s %s.", 
						dbUser.getFirstName(), 
						dbUser.getLastName()));
		} else {
			traceService.trace("Failed to find user by email.");
			return;
		}
		
		// Update using non-managed version
		u.setFirstName("invalid-name");
		try {
			userService.update(u);
		} catch (Exception e) {
			traceService.trace("An exception has occured: " + e.getMessage());
		}

	}
	
	/**
	 * Demo #1: DAO functions:
	 * - save User
	 * - findAll
	 * - findByEmal
	 */
	private void demo001() {
		traceService.nl();
		traceService.trace("Demo 001: Persistent api - save / findAll / findByEmail");
		User admin = new User("admin", "admin", "admin@gmail.com", 1, true, 1, null);
		userService.save(admin);
		
		User john = new User("John", "Doe", "djohn@gmail.com", 1, true, 1, null);
		userService.save(john);
		
		List<User> users = userService.findAll();
		
		traceService.nl();
		traceService.trace("******** Users (findAll) ********");
		users.stream().forEach(u -> {
			traceService.trace(
				String.format("User Name: %s %s", u.getFirstName(), u.getLastName()));
		});
		
		traceService.nl();
		User user1 = userService.findByEmail("djohn@gmail.com");
		
		traceService.nl();
		if(user1 != null) {
			traceService.trace("******** User (findByEmail) ********");
			traceService.trace(
				String.format("User Name: %s %s", user1.getFirstName(), user1.getLastName()));	
		} else {
			traceService.trace("findByEmail return no result.");
		}

	}
	
}