package edu.mum.util;

public interface TraceService {
	void trace(String text);
	void nl();
}
