package edu.mum.util;

import org.springframework.stereotype.Service;

@Service
public class TraceServiceImpl implements TraceService {

	
	@Override
	public void trace(String text) {
		System.out.println(text);
	}
	
	@Override
	public void nl() {
		System.out.println();
	}
}
