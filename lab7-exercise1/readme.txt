1. Initialize project -> ok
2. Run Populate.main() -> ok (inserted 1000000 Owner entries, 10 Pet entries per Owner)
3. Run App.main() -> ok -> 57056 ms

a. Add the @LazyCollection with option EXTRA to the association and run App again.
  -> ok -> 26952 ms
  
b) Remove the @LazyCollection, and modify the mapping for Owner.java to use batch 
fetching, batch size 10. Also check the time when using sizes 5 and 50.
- size = 5 -> ok -> 31366 ms
- size = 50 -> ok -> 22647 ms

c) Modify the mapping to use the sub-select strategy instead of batch fetching.
-> ok -> 23036 ms


d) Remove the sub-select strategy and use a join fetch query in App.java to retrieve 
everything. Also check the difference between using a named query, or just a query 
directly in code.
-> ok -> 25426 ms

e) Lastly modify the application to use an Entity Graph instead of a join fetch.
-> ok -> 23422 ms

Check to see if the strategy you thought would perform best was indeed the best 
for this situation. Remember, just because a strategy performed well under these 
circumstances does not necessarily mean it will perform well under other 
circumstances.