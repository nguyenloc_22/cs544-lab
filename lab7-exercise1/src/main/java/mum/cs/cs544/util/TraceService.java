package mum.cs.cs544.util;

public interface TraceService {
	void trace(String text);
	void nl();
}
