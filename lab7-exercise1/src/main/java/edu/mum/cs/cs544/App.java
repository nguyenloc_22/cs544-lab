package edu.mum.cs.cs544;

import java.util.List;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import mum.cs.cs544.util.TraceService;
import mum.cs.cs544.util.TraceServiceImpl;

public class App {

    private EntityManagerFactory emf;
    private TraceService traceService;
    
    private App() {
    	emf = Persistence.createEntityManagerFactory("cs544");
        traceService = new TraceServiceImpl();
    }

    public static void main(String[] args) throws Exception {
        
    	new App().runApp();
    	
        System.exit(0);
    }
    
    private void runApp() {
    	
    	// testOptimization();
    	
    	// testJoinFetch();
    	
    	testEntityGraph();
    	
    }
    
    /**
     * Lastly modify the application to use an Entity Graph 
     * instead of a join fetch.
     */
    private void testEntityGraph() {
    	long start = System.nanoTime();
    	
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        EntityGraph<Owner> graph = em.createEntityGraph(Owner.class);
        //graph.addAttributeNodes("name");
        graph.addSubgraph("pets");
        //.addAttributeNodes("name");
        
        TypedQuery<Owner> query = em.createQuery("from Owner", Owner.class);
        query.setHint("javax.persistence.fetchgraph", graph);
                
        List<Owner> ownerlist = query.getResultList();
        for (Owner o : ownerlist) {
            o.getPets().size();
        }
        
        Owner firstOwner = ownerlist.get(0);
        if(firstOwner != null) {
        	traceService.trace(String.format("First owner: %s. Nb of pets: %d",
        		firstOwner.getName(), firstOwner.getPets().size()));
        	
        	Pet firstPet = firstOwner.getPets().get(0);
        	if(firstPet != null) {
        		traceService.trace("First pet -> " + firstPet.getName());
        	}
        }
        
        em.getTransaction().commit();
        em.close();
        long stop = System.nanoTime();

        // stop time
        System.out.println("To fetch this data from the database "
        		+ "(using Entity Graph) took " 
    			+ (stop - start) / 1000000 
    			+ " milliseconds.");
    }
    
    @SuppressWarnings("unused")
	private void testJoinFetch() {
    	long start = System.nanoTime();
    	
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        TypedQuery<Owner> query = em.createQuery(
        		"from Owner o "
        		//+ "join fetch o.name name "
        		+ "join fetch o.pets pet", Owner.class);
        
        List<Owner> ownerlist = query.getResultList();
        for (Owner o : ownerlist) {
            o.getPets().size();
        }
        
        Owner firstOwner = ownerlist.get(0);
        if(firstOwner != null) {
        	traceService.trace(String.format("First owner: %s. Nb of pets: %d",
        		firstOwner.getName(), firstOwner.getPets().size()));
        	
        	Pet firstPet = firstOwner.getPets().get(0);
        	if(firstPet != null) {
        		traceService.trace("First pet -> " + firstPet.getName());
        	}
        }

        em.getTransaction().commit();
        em.close();
        long stop = System.nanoTime();

        // stop time
        System.out.println("To fetch this data from the database "
        		+ "(using join fetch) took " 
    			+ (stop - start) / 1000000 
    			+ " milliseconds.");	
    }
    
    @SuppressWarnings("unused")
	private void testOptimization() {
    	
        long start = System.nanoTime();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        TypedQuery<Owner> query = em.createQuery("from Owner", Owner.class);
        List<Owner> ownerlist = query.getResultList();
        for (Owner o : ownerlist) {
            o.getPets().size();
        }

        em.getTransaction().commit();
        em.close();
        long stop = System.nanoTime();

        // stop time
        System.out.println("To fetch this data from the database took " + (stop - start) / 1000000 + " milliseconds.");
    }

}
