package edu.mum.cs.cs544;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mum.cs.cs544.util.TraceService;
import mum.cs.cs544.util.TraceServiceImpl;

public class Populate {

    private static EntityManagerFactory emf;
    private static TraceService traceService = new TraceServiceImpl();
    
    public static void main(String[] args) throws Exception {
        emf = Persistence.createEntityManagerFactory("cs544");

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        for (int x = 0; x < 100000; x++) {
            Owner owner = new Owner("Frank" + x);
            List<Pet> petlist = new ArrayList<Pet>();
            for (int y = 0; y < 10; y++) {
                Pet pet = new Pet("Garfield" + x + "-" + y);
                petlist.add(pet);
            }
            owner.setPets(petlist);
            em.persist(owner);
            
            if(x%1000 ==0) {
            	traceService.trace(String.format("Inserted %d Owner into db.", x));
            }
        }

        em.getTransaction().commit();
        em.close();
        
        traceService.trace("Done!");
    }

}
