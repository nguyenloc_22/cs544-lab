package edu.mum.cs544.bank;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("edu.mum.cs544.bank")
//@ComponentScans({
//	@ComponentScan("edu.mum.cs544.bank"),
//	@ComponentScan("edu.mum.cs544.bank.dao"),
//	@ComponentScan("edu.mum.cs544.bank.jms"),
//	@ComponentScan("edu.mum.cs544.bank.logging"),
//	@ComponentScan("edu.mum.cs544.bank.service")
//})
public class Config {

}